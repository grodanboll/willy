# Willy Wunder

## How to run

Install node with version at or above 0.10.x.
Install gulp with version at or above 3.9.x.

Next, install the local dependencies Web Starter Kit requires:

```sh
$ npm install
```

Then run gulp to build the code and put in the /dist directory. 
The site will run on http://localhost:3000 as default.

```sh
$ gulp serve
```

That's it! 

The code is based on  the [Web Starter Kit](https://github.com/google/web-starter-kit/releases/latest).
More docs on Web Starter Kit are located in the /docs directory.
The code will build to the 
